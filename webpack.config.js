const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const {VueLoaderPlugin} = require("vue-loader");
const glob = require("glob-all");

/**
 * `..` Since this config file is in the config folder so we need
 * to resolve path in the top level folder.
 */
const resolve = relativePath => path.resolve(__dirname, ".", relativePath);

module.exports = {
    mode: "production",
    entry: {
        // This is where the `main-content` component is
        main: resolve("www/index.js"),
    },
    output: {
        filename: "[name].js",
        // Folder where the output of webpack's result go.
        path: resolve("www/dist"),
    },
    module: {
        rules: [
            {
                // vue-loader config to load `.vue` files or single file components.
                test: /\.vue$/,
                loader: "vue-loader",
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg|png|html|ico)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: "file-loader",
                    options: {
                        name: "[name].[ext]",
                        outputPath: ""
                    },
                }],
                exclude: [
                    resolve("node_modules/feather-icons")
                ]
            },
            {
                test: /\.svg$/,
                loader: "svg-inline-loader",
                include: [
                    resolve("node_modules/feather-icons")
                ]
            },
            {
                test: /\.css?$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader"
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader", // translates CSS into CommonJS
                    "sass-loader" // compiles Sass to CSS, using Node Sass by default
                ],
                include: [
                    resolve("www/css"),
                ],
            },
            {
                // This is required for other javascript you are gonna write besides vue.
                test: /\.js$/,
                loader: "babel-loader",
                include: [
                    resolve("www")
                ]
            },
        ],
    },
    plugins: [
        new VueLoaderPlugin(),
        new MiniCssExtractPlugin({
            filename: "style.css"
        }),
    ],
    resolve: {
        /**
         * The compiler-included build of vue which allows to use vue templates
         * without pre-compiling them
         */
        modules: [
            resolve("./www/"),
            resolve("./node_modules/"),
        ],
        alias: {
            "vue$": "vue/dist/vue.esm.js",
        },
        extensions: ["*", ".vue", ".js", ".json"],
    },
    // webpack outputs performance related stuff in the browser.
    performance: {
        hints: false,
    },
};