import Root from "../View/Root";
import Login from "../View/Login";
import Main from "../View/Main";
import Register from "../View/Register";
import Organization from "../View/Organization";
import Time from "../View/Time";
import Projects from "../View/Projects";
import TimeHistory from "../View/TimeHistory";
import OrganizationMembers from "../View/OrganizationMembers";
import OrganizationMemberTime from "../View/OrganizationMemberTime";
import OrganizationMember from "../View/OrganizationMember";

export default [
    {
        path: "/",
        component: Root,
        props: {
            currentView: Login,
            settings: {
                showNavbar: false
            }
        }
    },
    {
        path: "/main",
        component: Root,
        props: {
            currentView: Main,
            settings: {
                showNavbar: true
            }
        }
    },
    {
        path: "/register",
        component: Root,
        props: {
            currentView: Register,
            settings: {
                showNavbar: false
            }
        }
    },
    {
        path: "/register/:inviteCode",
        component: Root,
        props: {
            currentView: Register,
            settings: {
                showNavbar: false
            }
        }
    },
    {
        path: "/organization/:organizationId",
        component: Root,
        props: {
            currentView: Organization,
            settings: {
                showNavbar: true
            }
        }
    },
    {
        path: "/organization/:organizationId/time/:day/:month/:year",
        component: Root,
        props: {
            currentView: Time,
            settings: {
                showNavbar: true
            }
        }
    },
    {
        path: "/organization/:organizationId/projects",
        component: Root,
        props: {
            currentView: Projects,
            settings: {
                showNavbar: true
            }
        }
    },
    {
        path: "/organization/:organizationId/history/:year/:week",
        component: Root,
        props: {
            currentView: TimeHistory,
            settings: {
                showNavbar: true
            }
        }
    },
    {
        path: "/organization/:organizationId/members",
        component: Root,
        props: {
            currentView: OrganizationMembers,
            settings: {
                showNavbar: true
            }
        }
    },
    {
        path: "/organization/:organizationId/member/:userId",
        component: Root,
        props: {
            currentView: OrganizationMember,
            settings: {
                showNavbar: true
            }
        }
    },
    {
        path: "/organization/:organizationId/member/:userId/time/:year/:week",
        component: Root,
        props: {
            currentView: OrganizationMemberTime,
            settings: {
                showNavbar: true
            }
        }
    },
]
