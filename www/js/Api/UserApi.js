import Config from "../Utils/Config";

export default class UserApi {

    constructor(authToken) {
        this.authToken = authToken;
    }

    getUser(userId) {
        return fetch(Config.baseApiUrl + "/users/" + userId, {
            method: "GET",
            mode: "cors",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                "Authorization": "Bearer " + this.authToken
            }
        }).then((response) => {
            return response.json();
        })
    }
}