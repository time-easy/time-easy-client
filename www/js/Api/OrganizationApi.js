import Config from "../Utils/Config";

export default class OrganizationApi {
    constructor(authToken) {
        this.authToken = authToken;
    }

    getOrganizationsByUserId(userId) {
        return fetch(Config.baseApiUrl + "/users/" + userId + "/organizations", {
            method: "GET",
            mode: "cors",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                "Authorization": "Bearer " + this.authToken
            }
        }).then((response) => {
            return response.json();
        })
    }

    createOrganization(userId, organizationName) {
        const organizationObject = {
            name: organizationName
        };
        return fetch(Config.baseApiUrl + "/users/" + userId + "/organizations", {
            method: "POST",
            mode: "cors",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                "Authorization": "Bearer " + this.authToken
            },
            body: JSON.stringify(organizationObject)
        }).then((response) => {
            return response.json();
        })
    }

    getOrganization(organizationId) {
        return fetch(Config.baseApiUrl + "/organizations/" + organizationId, {
            method: "GET",
            mode: "cors",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                "Authorization": "Bearer " + this.authToken
            }
        }).then((response) => {
            return response.json();
        })
    }

    getOrganizationMembers(organizationId) {
        return fetch(Config.baseApiUrl + "/organization/" + organizationId + "/members", {
            method: "GET",
            mode: "cors",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                "Authorization": "Bearer " + this.authToken
            }
        }).then((response) => {
            return response.json();
        })
    }

    addMember(emailAddress, organizationId) {
        const memberInput = {
            email: emailAddress
        };
        return fetch(Config.baseApiUrl + "/organization/" + organizationId + "/members", {
            method: "POST",
            mode: "cors",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                "Authorization": "Bearer " + this.authToken
            },
            body: JSON.stringify(memberInput)
        }).then((response) => {
            return response.json();
        })
    }
}