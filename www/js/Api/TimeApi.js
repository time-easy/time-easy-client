import Config from "../Utils/Config";

export default class TimeApi {
    constructor(authToken) {
        this.authToken = authToken;
    }

    registerTime(date, projectId, hours, comment) {
        const timeEntryObject = {
            projectId: projectId,
            time: hours,
            comment: comment
        };
        return fetch(Config.baseApiUrl + "/project/" + projectId + "/time/" + date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear(), {
            method: "PUT",
            mode: "cors",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                "Authorization": "Bearer " + this.authToken
            },
            body: JSON.stringify(timeEntryObject)
        }).then((response) => {
            return response.json();
        })
    }

    getOwnTimeByDateAndOrganization(date, organizationId) {
        return fetch(Config.baseApiUrl + "/organization/" + organizationId + "/time/" + date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear(), {
            method: "GET",
            mode: "cors",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                "Authorization": "Bearer " + this.authToken
            }
        }).then((response) => {
            return response.json();
        })
    }

    getOwnTimeByOrganization(organizationId) {
        return fetch(Config.baseApiUrl + "/organization/" + organizationId + "/time", {
            method: "GET",
            mode: "cors",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                "Authorization": "Bearer " + this.authToken
            }
        }).then((response) => {
            return response.json();
        })
    }

    getTimeByUserAndOrganization(organizationId, userId) {
        return fetch(Config.baseApiUrl + "/organization/" + organizationId + "/member/" + userId, {
            method: "GET",
            mode: "cors",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                "Authorization": "Bearer " + this.authToken
            }
        }).then((response) => {
            return response.json();
        })
    }

}