import Config from "../Utils/Config";

export default class ProjectApi {
    constructor(authToken) {
        this.authToken = authToken;
    }

    getProjectsByOrganization(organizationId) {
        return fetch(Config.baseApiUrl + "/organization/" + organizationId + "/projects", {
            method: "GET",
            mode: "cors",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                "Authorization": "Bearer " + this.authToken
            }
        }).then((response) => {
            return response.json();
        })
    }

    createProject(organizationId, projectName) {
        const projectObject = {
            name: projectName
        };
        return fetch(Config.baseApiUrl + "/organization/" + organizationId + "/projects", {
            method: "POST",
            mode: "cors",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                "Authorization": "Bearer " + this.authToken
            },
            body: JSON.stringify(projectObject)
        }).then((response) => {
            return response.json();
        })
    }

}