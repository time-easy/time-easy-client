import Config from "../Utils/Config";

export default class {
    login(emailAddress, password) {
        const loginCredentials = {
            email: emailAddress,
            password: password
        };
        return fetch(Config.baseApiUrl + "/auth/authenticate", {
            method: "POST",
            mode: "cors",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(loginCredentials)
        }).then((response) => {
            return response.json();
        });
    }

    register(name, emailAddress, password) {
        const accountCredentials = {
            name: name,
            email: emailAddress,
            password: password
        };
        return fetch(Config.baseApiUrl + "/auth/register", {
            method: "POST",
            mode: "cors",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(accountCredentials)
        }).then((response) => {
            return response.json();
        });
    }
}