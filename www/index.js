import Vue from "vue"
import Vuetify from "vuetify"
import "vuetify/dist/vuetify.css"
import RoutingConfig from "./js/Utils/RoutingConfig"
import "html/index.html"
import "css/index.scss"
import "@fortawesome/fontawesome-free/css/all.min.css"
import VueRouter from "vue-router"
import "img/time-easy-logo.svg"
import "img/favicon.ico"

//Using the fishbowl pallette: https://www.colourlovers.com/palette/4645385/Fishbowl

Vue.use(Vuetify, {
    iconfont: "fa",
    theme: {
        primary: "#428CB8",
        secondary: "#CB770A"
    }
});

Vue.use(VueRouter);

// Cordova app
let app = {
    initialize: () => {}
};

app.initialize();

const router = new VueRouter({
    mode: "hash",
    routes: RoutingConfig
});

// Vue app
const vueApp = new Vue({
    router,
    el: "#app"
});
