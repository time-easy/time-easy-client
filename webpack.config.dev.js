const path = require("path");
const {VueLoaderPlugin} = require("vue-loader");

/**
 * `..` Since this config file is in the config folder so we need
 * to resolve path in the top level folder.
 */
const resolve = relativePath => path.resolve(__dirname, ".", relativePath);

module.exports = {
    mode: "development",
    entry: {
        // This is where the `main-content` component is
        main: resolve("www/index.js"),
    },
    output: {
        filename: "[name].js",
        // Folder where the output of webpack's result go.
        path: resolve("www/dist"),
    },
    module: {
        rules: [
            {
                // vue-loader config to load `.vue` files or single file components.
                test: /\.vue$/,
                loader: "vue-loader",
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg|png|html|ico)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: "file-loader",
                    options: {
                        name: "[name].[ext]",
                        outputPath: ""
                    },
                }],
                exclude: [
                    resolve("node_modules/feather-icons")
                ]
            },
            {
                test: /\.svg$/,
                loader: "svg-inline-loader",
                include: [
                    resolve("node_modules/feather-icons")
                ]
            },
            {
                test: /\.css?$/,
                use: [
                    "css-loader"
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    "css-loader", // translates CSS into CommonJS
                    "sass-loader" // compiles Sass to CSS, using Node Sass by default
                ],
                include: [
                    resolve("www/css"),
                ],
            },
            {
                // This is required for other javascript you are gonna write besides vue.
                test: /\.js$/,
                loader: "babel-loader",
                include: [
                    resolve("www"),
                    resolve("www/api")
                ]
            },
        ],
    },
    /**
     * There are multiple devtools available, check
     * https://github.com/webpack/webpack/tree/master/examples/source-map
     */
    devtool: "source-map",
    devServer: {
        // The url you want the webpack-dev-server to use for serving files.
        host: "localhost",
        // Can be the popular 8080 also.
        port: 8010,
        // gzip compression
        compress: true,
        // Open the browser window, set to false if you are in a headless browser environment.
        open: false,
        watchOptions: {
            ignored: /node_modules/,
            poll: true,
        },
        // The path you want webpack-dev-server to use for serving files
        publicPath: "",
        // For static assets
        contentBase: resolve("www/dist"),
        // Reload for code changes to static assets.
        watchContentBase: true,
    },
    plugins: [
        new VueLoaderPlugin(),
    ],
    resolve: {
        /**
         * The compiler-included build of vue which allows to use vue templates
         * without pre-compiling them
         */
        modules: [
            resolve("./www/"),
            resolve("./node_modules/"),
        ],
        alias: {
            "vue$": "vue/dist/vue.esm.js",
        },
        extensions: ["*", ".vue", ".js", ".json"],
    },
    // webpack outputs performance related stuff in the browser.
    performance: {
        hints: false,
    },
};